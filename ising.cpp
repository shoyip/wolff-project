#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <cmath>
#include <vector>
#include <random>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

random_device rseed;
mt19937 rgen(rseed());
uniform_real_distribution<double> rdist(0., 1.);
uniform_int_distribution<int> idist(0, 1);

int mod (int a, int b) { return ((a%b)+b)%b; };

int count (vector<bool> vec) {
    int sum_of_elems;
    for (bool n : vec) {
        sum_of_elems += n;
    }
    return sum_of_elems;
}

class Lattice {
    private:
        vector<int> lattice;
        int L;
        double J, T;
        double Mag, Erg;
        int l;
        double deltaE;
        double beta;
        double addp;
        int rs;
        double accepted=0;
        double totalruns=0;
        vector<bool> cluster;
        ofstream outf;
        ofstream outf1;
        int curracc = 0;
        int currspin;
        int clustercount;
        int oldspin;
        int firstround;
    public:
        Lattice (int start, int L_in, double J_in, double T_in) {
            L = L_in;
            J = J_in;
            T = T_in;
            beta = 1./T;
            if (start==1) {
                for (int i=0; i<L*L; i++) lattice.push_back(1);
            } else if (start==0) {
                for (int i=0; i<L*L; i++) {
                    idist(rgen)==1 ? lattice.push_back(1) : lattice.push_back(-1);
                }
            }
            firstround = 1;
            Mag = 0;
        }

        void setTemp (double newTemp) {
            T = newTemp;
        }

        int right(int i) {return (i/L)*L + mod((i+1), L);};
        int down(int i) {return mod((i+L), L*L);};
        int left(int i) {return (i/L)*L + mod((i-1), L);};
        int up(int i) {return mod((i-L), L*L);};
        
        vector<int> neighborArr (int i) {
            vector<int> arr;
            arr.push_back(this->right(i));
            arr.push_back(this->down(i));
            arr.push_back(this->left(i));
            arr.push_back(this->up(i));
            return arr;
        }

        int neighborSum (int i) {
            int sum = 0;
            for (auto &neighbor : this->neighborArr(i))
                sum += lattice[neighbor];
            return sum;
        }
        
        void updatemc () {
            totalruns += 1.;
            l = rand() % (L*L);
            currspin = lattice[l];
            deltaE = 2.*(J*this->neighborSum(l))*currspin;
            if (deltaE<0) {
                curracc = 1;
                accepted += 1.;
                lattice[l] *= -1;
            } else if (rdist(rseed) < exp(-beta*deltaE)) {
                curracc = 1;
                accepted += 1.;
                lattice[l] *= -1;
            } else {
                curracc = 0;
            }
        }

        void updatewolff () {
            addp = 1. - exp(-2.*J*beta);
            l = rand() % (L*L);
            vector<int> stack;
            stack.push_back(l);
            oldspin = lattice[l];
            lattice[l] *= -1;
            int current;
            clustercount = 1;
            while (!stack.empty()) {
                current = stack.back();
                stack.pop_back();
                for (auto &neighbor : this->neighborArr(current))
                    if (lattice[neighbor]==oldspin and rdist(rseed)<addp) {
                        stack.push_back(neighbor);
                        lattice[neighbor] *= -1;
                        clustercount++;
                    }
            }
        }
        
        void mcupdatesinglemag(int t) {
            if (firstround==1) {
                for (auto &spin : lattice) Mag+=spin;
                firstround=0;
            } else if (curracc==1) {
                Mag-=currspin*2;
            }
            //cout << "mag updated!" << endl;
        }
        
        void wolffupdatesinglemag(int t) {
            if (firstround==1) {
                for (auto &spin : lattice){
                    Mag+=spin;
                }
                cout << "starting magentization is " << Mag << endl;
                firstround=0;
            } else {
                Mag-=oldspin*clustercount*2;
                //cout << Mag << endl;
            }
        }

        double mag () {
            //Mag = 0.;
            //for (auto &spin : lattice) Mag+=spin;
            //cout << "magnetization is "<< Mag/(L*L) << endl;
            //cout << "magentization per site is " << Mag/(L*L) << endl;
            return Mag/(L*L);
        }
        
        double erg () {
            Erg = 0.;
            for (int i=0; i<lattice.size(); i++) {
                Erg += -lattice[i]*(this->neighborSum(i)*J);
            }
            return Erg/(L*L);
        }
        
        void recordOpen (char const* filename, auto xvar, int sample) {
            outf.open(filename);
            string magstr ("mag");
            string ergstr ("erg");
            string undstr ("_");
            char buf [10];
            stringstream ss1;
            stringstream ss2;
            ss1 << xvar;
            ss2 << sample;
            outf << "t," << magstr << ss1.rdbuf() << undstr << ss2.rdbuf() << endl;
        }
        void recordClose () { outf.close(); };
        void record (int t) {
            outf << t << "," << this->mag() << endl;
        }
        double accRatio () {
            return accepted / totalruns;
        }

        void print () {
            for (int i=0; i<L*L; i++) {
                if (lattice[i]==1) cout << "██";
                if (lattice[i]==-1) cout << "░░";
                if (mod(i,L)==L-1) cout << endl;
            }
            cout << endl;
        }

        void create_images (int t, double temp) {
            outf1.open("image.txt");
            for (auto &spin : lattice) {
                outf1 << spin << ",";
            }
            outf1.close();
            string mvcom3 = "mv image.txt ./images/image_" + to_string(temp) + "_" + to_string(t) + ".txt";
            system(mvcom3.c_str());
        }
};

int main (int argc, char *argv[]) {
//    vector<int> lengths {5, 20, 80, 100};
    vector<int> lengths {100};
//    vector<double> temps {0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.0, 2.1, 2.2, 2.3, 2.4, 2.7, 3.0, 3.3, 3.6, 3.9};
//    vector<double> temps {0.3, 2.2691853, 4};
    vector<double> temps {2.2691853};

    int run = 0;
    int meas;
    int wolff;
    system("rm -rf data");
    system("mkdir -p data");
    system("rm -rf images");
    system("mkdir -p images");
    cout << "Choose measurement to be made ([0] mag per size [1] mag per temp):" << endl;
    cin >> meas;
    cout << "Choose algorithm ([0] Metropolis [1] Wolff):" << endl;
    cin >> wolff;
    int wolff_sweeps;
    int mc_sweeps;
    int wolff_strec;
    int mc_strec;
    int sweeps;
    int strec;
    if (meas==0) {
        double Tc = 2.269185;
        wolff_sweeps = 50;
        mc_sweeps = 10000000;
        wolff_strec = 40;
        mc_strec = 1000000;
        sweeps = wolff==0 ? mc_sweeps : wolff_sweeps;
        strec = wolff==0 ? mc_strec : wolff_strec;
        //cout << "sweeps are " << sweeps << endl;
        for (auto &length : lengths) {
            double T0 = .5*Tc;
            double T = T0;
            for (int sample=0; sample<1; sample++) {
                Lattice grid(1, length, 1., T0);
                cout << "length is " << length << " for sample n. " << sample << endl;
                grid.recordOpen("size.csv", length, sample);
                int t = 0;
                for (int sweep=0; sweep<sweeps; sweep++) {
                    if (T < Tc and sweep%5==0) {
                        T += .1;
                        grid.setTemp(T);
                    } else if (T < Tc and sweep%5!=0){
                        continue;
                    } else {
                        T = Tc;
                        grid.setTemp(T);
                    }
                    for (int t_sweep=0; t_sweep<(length*length); t_sweep++) {
                        if (wolff==0) {
                            grid.updatemc();
                            t++;
                            if (t>=mc_strec) {
                                grid.mcupdatesinglemag(t);
                            }
                        } else if (wolff==1) {
                            grid.updatewolff();
                            t++;
                            if (t>=wolff_strec) {
                                grid.wolffupdatesinglemag(t);
                            }
                        }
                        if (sweep>=(wolff==0 ? mc_strec : wolff_strec) and t%100==0) {
                            grid.record(t);
                        }
                    }
                }
                cout << "acceptance ratio is " << grid.accRatio() << endl;
                grid.recordClose();
                string mvcom0 = "mv size.csv ./data/size" + to_string(length) + "_" + to_string(sample) + ".csv";
                system(mvcom0.c_str());
            }
            run++;
        }
    } else if (meas==1) {
        int length = 100;
        wolff_sweeps = 1010; // * 100, time in wolff is not the same
        mc_sweeps = 10000010;
        wolff_strec = 1000; // * 100
        mc_strec = 10000000;
        sweeps = wolff==0 ? mc_sweeps : wolff_sweeps;
        strec = wolff==0 ? mc_strec : wolff_strec;
        cout << "sweeps are " << sweeps << endl;
        for (auto &temp : temps) {
            cout << "temp is " << temp << endl;
            for (int sample=0; sample<5; sample++) {
                Lattice grid(1, length, 1., temp); // 1 cold start 0 hot start
                int t=0;
                grid.recordOpen("temp.csv", temp, sample);
//                for (int sweep=0; sweep<sweeps; sweep++) {
                while (t<sweeps) {
                    //cout << sweep << " sweeps" << endl;
//                    for (int t_sweep=0; t_sweep<(length*length); t_sweep++) {
                        cout << "t is " << t << endl;
                        if (wolff==0) {
                            grid.updatemc();
                            t++;
                            if (t>=mc_strec) {
                                grid.mcupdatesinglemag(t);
                            }
                        } else if (wolff==1) {
                            grid.updatewolff();
                            t++;
                            if (t>=wolff_strec) {
                                grid.wolffupdatesinglemag(t);
                            }
                        }
                        //grid.print();
                    //}
                    if (t>=(wolff==0 ? mc_strec : wolff_strec) and t%1==0) {
                        cout << "recording" << endl;
                        grid.record(t);
                    }
                    if (t>wolff_strec and t<wolff_sweeps)
                        grid.create_images(t, temp);
                    if (t>wolff_sweeps)
                        break;
                }
                //cout << "acceptance ratio is " << grid.accRatio() << endl;
                grid.recordClose();
                string mvcom1 = "mv temp.csv ./data/temp" + to_string(temp) + "_" + to_string(sample) + ".csv";
                system(mvcom1.c_str());
            }
            run++;
        }
    }
    if (meas==0) {
        if (wolff==0) {
            system("paste -d\",\" ./data/size*.csv > sizemc.csv");
        } else if (wolff==1) {
            system("paste -d\",\" ./data/size*.csv > sizewolff.csv");
        }
    } else if (meas==1) {
        if (wolff==0) {
            system("paste -d\",\" ./data/temp*.csv > tempmc.csv");
        } else if (wolff==1) {
            system("paste -d\",\" ./data/temp*.csv > tempwolff.csv");
        }
    }
//    system("rm -rf data");
}
