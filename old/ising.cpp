#include <iostream>
#include <random>
#include <fstream>
#include <cmath>
#include <vector>

using namespace std;

// initialize random device and distributions
random_device rseed;
mt19937 rgen(rseed());
uniform_real_distribution<double> rdist(0., 1.);
uniform_int_distribution<int> idist(0,1);

int mod (int a, int b) {
    return ((a%b)+b)%b;
}

vector<int> get_neighbors (int L, int N) {
    vector <int> nbr;
    for (int i=0; i<N; i++) {
        for (int j=0; j<4; j++) {
            if (j==0) nbr.push_back((i/L)*L + mod((i+1), L));
            if (j==1) nbr.push_back(mod((i+L),N));
            if (j==2) nbr.push_back((i/L)*L + mod((i-1), L));
            if (j==3) nbr.push_back(mod((i-L),N));
        }
    }
    return nbr;
}

double sum (vector<int> array) {
    double a=0.;
    for (int i=0; i<array.size(); i++) a+=array[i];
    return a;
}

double mean (vector<double> &array) {
    double a=0.;
    for (int i=0; i<array.size(); i++) a+=array[i];
    return a/float(array.size());
}

vector<int> slice(vector<int> array, int a, int b) {
    auto start = array.begin() + a;
    auto end = array.begin() + b + 1;
    vector<int> result(b-a+1);
    copy(start, end, result.begin());
    return result;
}

vector<double> squarev (vector<double> &array) {
    vector <double> sqarray;
    for (int i=0; i<array.size(); i++) {
        sqarray.push_back(array[i]*array[i]);
    }
    return sqarray;
}

void update (vector<int> &lattice, vector<int> neighbors, double J, double B,
        double beta, int N) {
    int l = rand() % N;
    double sumnbr = 0;
    for (int j=0; j<4; j++) sumnbr += lattice[neighbors[l*4+j]];
    double deltaE = -2. * (J * sumnbr + B) * lattice[l];
    if (rdist(rseed) < exp(-beta*deltaE)) lattice[l] *= -1;
}

void measure (vector<int> lattice, vector<int> neighbors, int N, int mfreq,
        int t, int ntime, double J, double B, vector<double> &mag,
        vector<double> &erg, vector<double> &window, vector<double> &acorr,
        vector<double> &tavec) {
    int mstep = t/mfreq;
    // calculate magentization
    double magnetization = double(sum(lattice))/N;
    // calculate energy
    vector<int> erglatt;
    for (int i=0; i<lattice.size(); i++) {
        double sumnbr = 0;
        for (int j=0; j<4; j++) sumnbr += lattice[neighbors[i*4+j]];
        erglatt.push_back(-lattice[i]*(sumnbr*J+B));
    }
    double energy = double(sum(erglatt));
    // calculate autocorrelation of magnetization
    if (mstep < ntime) {
        window.push_back(energy);
    } else {
        tavec.push_back(t*1./N);
        window.erase(window.begin());
        window.push_back(energy);
        for (int j=0; j<ntime-1; j++) {
            acorr[j] += double(window[0]*window[j]);
        }
    }
    // push magentization and energy values
    mag.push_back(magnetization);
    erg.push_back(energy);
}

void write (vector<double> x, char const* filename) {
    ofstream outf;
    outf.open(filename);
    for (int i=0; i<x.size(); i++) {
        outf << x[i] << endl;
    }
    outf << endl;
    outf.close();
}

void print_grid (vector<int> lattice, int L, int N) {
    for (int i=0; i<N; i++) {
        if (lattice[i]==1) cout << "██";
        if (lattice[i]==-1) cout << "░░";
        if (mod(i,L)==L-1) cout << endl;
    }
}

int main(int argc, char *argv[]) {
    // initialize variables and parameters
    int L = atoi(argv[1]);
    double T0 = atof(argv[2]);
    char const *mag_filename = argv[3];
    char const *erg_filename = argv[4];
    char const *corr_filename = argv[5];
    int steps = 1e7; // number of mc steps
    long int N = L*L; // dimension of lattice
    double J = 1; // short-range potential
    double B = 0; // long-range potential
    //double T0 = 0.001*2.26919; // temp in terms of critical temp
    double beta = 1./T0;
    //vector<int> lattice(N, 1); // initialize lattice (cold start)
    vector<int> lattice; // initialize lattice (hot start)
    for (int i=0; i<N; i++) idist(rgen)==1 ? lattice.push_back(1) : lattice.push_back(-1);
    vector<int> neighbors; // neighbor of each lattice point (rdlu)
    neighbors = get_neighbors(L, N); // initialize neighbor list

    // initialize auxiliary variables
    int ntime = 10000; // fixed autocorr length
    vector<double> acorr(ntime-1, 0.);
    vector<double> window;

    // initialize measuring variables and parameters
    vector<double> mag; // magnetization values array
    vector<double> erg; // energy values array
    int nobs = 100000; // number of saving points
    int mfreq = steps/nobs; // measure every mfreq
    vector<double> tvec;
    vector<double> tavec;

    // start mc loop
    for (int t=0; t<steps; t++) {
        update(lattice, neighbors, J, B, beta, N);
        if (t%mfreq==0) {
            tvec.push_back(t*1./N);
            measure(lattice, neighbors, N, mfreq, t, ntime, J, B, mag, erg,
                    window, acorr, tavec);
        }
    }

    // write outputs to files
    write(mag, mag_filename);
    write(erg, erg_filename);
    write(acorr, corr_filename);
    write(tvec, "tvec.csv");
    write(tavec, "tavec.csv");
}
