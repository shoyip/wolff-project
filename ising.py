import numpy as np
import random

class Lattice:
    def __init__(self, cold, L, J, B, T):
        self._L = L
        self._J = J
        self._B = B
        self._T = T
        if (cold==True):
            self._grid=(np.array([1 for j in range(L*L)]))
        else:
            self._grid=(np.array([random.choice([-1, 1]) for j in range(L*L)]))
    def updatemc():
        l = random.randrange(_L*_L)
        nbr = np.array([grid[(l//L)*L + (l+1)%_L],
           grid[(l+1)%(_L*_L)],
           grid[(l//_L)*_L + (l-1)%_L],
           grid[(l-_L)%(_L*_L)]])
        
lattice = Lattice(True, 10, 1., 0., 1.6)
print(lattice)
# L = 100
# lattice = np.array([1 for j in range(L*L)])
# tmax = 100000
# T = 3.5
# beta = 1./T
# J = 1.
# mag = []
# for t in range(tmax):
#     l = random.randrange(L*L)
#     nbr = np.array([lattice[(l//L)*L + (l+1)%L],
#            lattice[(l+1)%(L*L)],
#            lattice[(l//L)*L + (l-1)%L],
#            lattice[(l-L)%(L*L)]])
#     deltaE = -2.*lattice[l]*(J*nbr.sum())
#     if (random.random() < np.exp(-beta*deltaE)):
#         lattice[l] *= -1
#     if (t%100==0):
#         mag.append(np.sum(lattice))