#!/bin/bash
#temps=( 0.6 1.2 1.4 1.5 1.6 1.7 1.8 2.1 2.4 2.7 3.0 3.3 )
#temps=( 0.4 0.6 0.8 1.0 1.2 1.4 1.5 1.6 1.7 1.8 2.0 2.2 2.4 2.8 3.0 3.4 )
sizes=( 5 8 10 12 )
rm -Rf preproc
mkdir -p preproc/{mag,erg,corr}
n=0
for size in "${sizes[@]}"
do
    ./ising $size 2.269 preproc/mag/$n.csv preproc/erg/$n.csv preproc/corr/$n.csv
    let "n+=1"
    echo $n
done
paste -d"," tvec.csv preproc/mag/* > mag.csv
paste -d"," tvec.csv preproc/erg/* > erg.csv
paste -d"," tavec.csv preproc/corr/* > corr.csv
