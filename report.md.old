# Introduction

## Ising model

The Ising model is a model of a magnet. The model postulates a lattice
with a magnetic dipole or spin on each site. Spins assume the form of scalar
variables $s_i$ which can take only values $\pm 1$.

We will study the case of the two-dimensional Ising model on a square lattice
with $N$ sites and side length $L=\sqrt{N}$, and the Hamiltonian will have the
following form
$$
\mathcal{H} = - \sum_{\expval{ij}}^N J s_i s_j - H \sum_i^N s_i,
$$ {#eq:hamilt}
where the sum over $\expval{ij}$ implies the sum over all pairs of
nearest-neighbor sites (the adjacent sites of the $i$ spin in the upward,
downward, left and right positions), $J$ is the *coupling* between neighboring
sites and $H$ is the external magnetic field applied to the system. In our
following studies we will assume zero magnetic field, hence $H=0$.

The minus sign in @eq:hamilt is merely conventional: in this case, called
*ferromagnetic model*, for $J>0$ aligned neighboring spins are energetically
favourable. 

In order to know the possible states of the Ising systems we will have to
enumerate the different sets of values that the spins can take. Each spin can
take two values, so the total number of states for a lattice with $N$ spins
amounts to $2^N$. The partition function of the model is
$$
Z = \sum_{\{s_i\}} e^{-\beta\mathcal{H}}
$$ {#eq:partf}
Once we are able to perform this sum, either analytically or with the aid of a
computer, then we can find other quantities of interest. We can evaluate the
*mean magnetization per spin* $\expval{m}$ as
$$
\expval{m} = \frac{1}{N} \expval{\sum_i s_i},
$$
and the *specific heat per spin*
$$
c = \frac{k\beta^2}{N} (\expval{E^2} - \expval{E}^2).
$$

## Solving the Ising model

We may ask ourselves which are the different ways one can solve for the
properties of the Ising model [@sethna2008]. Ernst Ising himself has solved
analytically the one-dimensional case [@ising1925]: his studies yielded to the
result that the solution *does not admit phase transitions*, as these are
located in points where the free energy $F$ is not analytic.

The Ising model undergoes a phase transition [@2020] between an ordered
and disordered phase (respectively what we called ferromagnetic and
non-ferromagnetic phase in the previous section) in two dimensions or more. This
model without the magnetic field term $B$ was studied in an analytical way by
Lars Onsager in an extremely convoluted manner [@onsager1944], but still the
two-dimensional system with the magnetic field and systems at higher dimension
do not have an analytical solutions.

Luckily numerical methods to solve this model have been developed over the
years, and in particular we will be studying Monte Carlo methods. This kind of
methods involve simulating a random trajectory through the space of lattice
configurations [@sethna2008].

## Markov chains

## Metropolis algorithm

# Observables

## Autocorrelation Function

# Finite size scaling

# Bibliography
