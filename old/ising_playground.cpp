#include <iostream>
#include <random>
#include <stdlib.h>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int mod (int a, int b) {
    return ((a%b)+b)%b;
}

vector<int> get_neighbors (int L, int N) {
    vector<int> nbr;
    for (int i=0; i<N; i++) {
        for (int j=0; j<4; j++) {
            if (j==0) nbr.push_back((i/L)*L+mod((i+1),L));
            if (j==1) nbr.push_back(mod((i+L),N));
            if (j==2) nbr.push_back((i/L)*L+mod((i-1),L));
            if (j==3) nbr.push_back(mod((i-L),N));
        }
    }
    return nbr;
}

void print_grid (vector<int> lattice, int L, int N) {
    for (int i=0; i<N; i++) {
        if (lattice[i]==1) cout << "██";
        if (lattice[i]==-1) cout << "░░";
        if (mod(i,L)==L-1) cout << endl;
    }
}

double mean (vector <double> &array) {
    double a=0;
    for (int i=0; i<array.size(); i++) a+=array[i];
    return a/float(array.size());
}

vector <double> squarev (vector <double> &array) {
    vector <double> sqarray;
    for (int i=0; i<array.size(); i++) {
        sqarray.push_back(array[i]*array[i]);
    }
    return sqarray;
}

int main (int argc, char *argv[]) {
    std::random_device rseed;
    std::mt19937 rgen(rseed());
    std::uniform_int_distribution<int> idist(0,1);

    int l;
    const int L = atoi(argv[1]);
    const long long int N = L*L;
    int steps = 1e7;

    // autocorr variables
    int ntime=2000;
    int count1=0;
    int count2=0;
    double aobs=0.;
    vector<double> acor(ntime-1,0.);
    vector<double> tobs;

    double T0, Tf, beta, E, M, Tstep;
    double J, B;
    J = 1;
    B = 0;
    T0 = 0.001*2.26;
    beta = 1. / T0;
    vector<int> lattice;

    // initialize lattice with random spins
    cout << "Initializing lattice..." << endl;
    //for (int i=0; i<N; i++) idist(rgen)==1 ? lattice[i]=1 : lattice[i]=-1;
    // cold initialization!
    for (int i=0; i<N; i++) lattice.push_back(-1);
    cout << "Initialized lattice" << endl;

    vector<int> neighbors;
    neighbors = get_neighbors(L, N);

    // start mc loop
    int sumnbr, magval;
    vector<double> mag;
    int const skip = steps/1e5+1;
    double randn, deltaE, avgmag;
    uniform_real_distribution<double> rdist (0., 1.);

    for (int i=0; i<steps; i++) {
        sumnbr = 0;
        l = rand() % N; // l is the spin to be flipped
        for (int j=0; j<4; j++) {
            sumnbr += lattice[neighbors[l*4+j]];
        }
        deltaE = -2. * (J*lattice[l] * sumnbr + B*lattice[l]);
        randn = rdist(rseed);
        if (randn < exp(-beta*deltaE)) {
            lattice[l] *= -1;
        }
        // record observables
        if (i%skip==0) {
            magval = 0;
            for (int k=0; k<lattice.size(); k++) {
                magval += lattice[k];
            }
            mag.push_back(magval*1.0/N);

            // autocorr calc
            if (count1<ntime) {
                count1++;
                tobs.push_back(magval*1./N);
            } else {
    //            for (int k=0; k<tobs.size(); k++) cout << tobs[k] << endl;
                count2++;
                aobs += magval;
                tobs.erase(tobs.begin());
                tobs.push_back(magval*1./N);
                for (int j=0; j<ntime-1;j++) {
                    acor[j]+=tobs[0]*tobs[j];
                }
            }
        }
    }
    cout << endl;

    // write to file
    ofstream obs;
    obs.open(argv[2]);
    int measstep, obsn;
    obsn = steps/skip;
    double mag0 = mag[0];
    double meanmag = mean(mag);
    double autocorr;
    vector<double> magsq = squarev(mag);
    double meanmagsq = mean(magsq);
    obs << "t,mag" << endl;
    for (int i=0; i<obsn; i++) {
        measstep = i*skip;
        obs << measstep << ",";
        obs << mag[i] << endl;
    }
    obs.close();
    vector<double> magmag0;
    int chunks = obsn/2000;
    cout << "mag size is " << mag.size() << endl;
    ofstream corrf1;
    corrf1.open("corr1.csv");
    corrf1 << "t,corr" << endl;
    for (int i=0; i<2000; i++) {
        magmag0.clear();
        for (int j=0; j<chunks; j++) {
            magmag0.push_back(mag[j*2000]*mag[j*2000+i]);
        }
        autocorr = ( mean(magmag0) - meanmag*meanmag )/( meanmagsq - meanmag*meanmag );
        corrf1 << i*skip << "," << autocorr << endl;
    }
    corrf1 << endl;
    corrf1.close();
    cout << mean(mag) << endl;

    // write corr to file
    ofstream corrf;
    corrf.open("corr.csv");
    corrf << "t,corr" << endl;
    for (int i=0; i<ntime-1; i++) {
        corrf << i*skip << "," << (acor[i]-meanmag*meanmag)/(meanmagsq-meanmag*meanmag) << endl;
    }
    corrf << endl;
    corrf.close();
}
