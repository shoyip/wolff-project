#include <iostream>
#include <random>

int main () {
    std::random_device rseed;
    std::mt19937 rgen(rseed()); // mersenne_twister
    std::uniform_int_distribution<int> idist(-1,1); // [0,100]
    
    std::cout << idist(rgen) << std::endl;
}
