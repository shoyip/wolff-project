# Metropolis, Wolff and ML Algorithms for the Ising model

The report is available at [report.pdf](report.pdf).

The code used for the simulations is in [ising.cpp](ising.cpp),
and the binary file is [ising](ising). Some parameters should be
tuned inside the program itself and are set to the last run simulation.

Part of the data analysis has been done on the [Data Analysis 1](Data Analysis 1.ipynb) notebook, and part of it has been done on the [Data Analysis Playground](Data Analysis Playground.ipynb) notebook.

The RBM parts of the analysis have been executed on a Google Colab notebook and
can be accessed from [this
link](https://colab.research.google.com/gist/shoyip/d41e85801bb5c3f666c9bab8b4644bb5/boettcher-rbm.ipynb).
The scripts and the functions used in the notebook are taken from Github user
`dngfra`'s repository [Restricted-Boltzmann-Machines](https://github.com/dngfra/Restricted-Boltzmann-Machines).
