using Printf
using Plots

const L = 20
const n_sweep = 20
const n_therm = 1000
const n_data = 100
const temps = 4.0:-0.3:0.1

function measure(i, energy, magnetization, s)
    en = 0
    m = 0
    for x = 1:L
        for y = 1:L
            u = 1+mod(y,L)
            r = 1+mod(x,L)
            en -= s[x,y]*(s[x,u]+s[r,y])
            m += s[x,y]
        end
    end
    energy[i] = en
    magnetization[i] = abs(m)
end

function flip(x, y, T, s)
    u = 1+mod(y,L)
    d = 1+mod(y-2,L)
    r = 1+mod(x,L)
    l = 1+mod(x-2,L)
    de = 2*s[x,y]*(s[x,u]+s[x,d]+s[l,y]+s[r,y])
    if(de<0)
        s[x,y] = -s[x,y]
    else
        p = rand()
        if(p<exp(-de/T))
            s[x,y] = -s[x,y]
        end
    end
end

function sweep(n, T, s)
    for i = 1:n
        for x = 1:L
            for y = 1:L
                flip(x,y,T,s)
            end
        end
    end
end

function main()
    e1 = Array(1:n_data)
    m1 = Array(1:n_data)
    et = []
    mt = []
    s = ones(Int32,L,L)
    for T in temps
        sweep(n_therm, T, s)
        energy = e1
        magnetization = m1
        for i = 1:n_data
            sweep(n_sweep,T,s)
            measure(i, energy, magnetization, s)
        end
        en_ave = sum(energy)/n_data
        ma_ave = sum(magnetization)/n_data
        push!(et, en_ave(L*L))
        push!(mt, ma_ave(L*L))
        @printf("%8.3f %8.3f \n", en_ave(L*L), ma_ave/(L*L))
    end
    plot(temps,mt)
end

main()
