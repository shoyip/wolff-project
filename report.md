# Introduction

## Ising model

The Ising model is a model of a magnet. The model postulates a lattice
with a magnetic dipole or spin on each site. Spins assume the form of scalar
variables $s_i$ which can take only values $\pm 1$.

![Schematic representation of spin lattices in the two dimensional Ising model,
in ferromagnetic (left) and paramagnetic phases (right) [^1]](ising_grid.pdf)

[^1]: Taken from [Laurin Pannullo](https://itp.uni-frankfurt.de/~mwagner/teaching/C_WS19/projects/Ising_proj.pdf)

We will study the case of the two-dimensional Ising model on a square lattice
with $N$ sites and side length $L=\sqrt{N}$, and the Hamiltonian will have the
following form
$$
\mathcal{H} = - \sum_{\expval{ij}}^N J s_i s_j - H \sum_i^N s_i,
$${#eq:hamilt}
where the sum over $\expval{ij}$ implies the sum over all pairs of
nearest-neighbor sites (the adjacent sites of the $i$ spin in the upward,
downward, left and right positions), $J$ is the *coupling* between neighboring
sites and $H$ is the external magnetic field applied to the system. In our
following studies we will assume zero magnetic field, hence $H=0$.

The minus sign in @eq:hamilt is merely conventional: in this case, called
*ferromagnetic model*, for $J>0$ aligned neighboring spins are energetically
favourable. 

The Ising model has exact solutions for one dimension [@ising1925] and for two
dimensions [@yang1952] with zero field. Althought in the first model it was
shown that no phase transition occurs, in the latter one a second order phase
transition is seen with divergences for observables such as specific heat,
susceptibility and correlation length [@landau2014].

In order to study the static and dynamic properties of the Ising model we can
simulate its evolution in time generating contiguous spin lattice states, thus
generating the trajectory in phase space. In our case we use spin-flip Markov
chain steps weighted over the Boltzmann distribution.

## Statistical Physics

Equilibrium statistical mechanics [@landau2014] is based upon the idea of a
partition function which encapsulates all necessary informations about the
system taken into account. Generally
$$
Z = \Tr (\exp(-\mathcal{H})) = \sum_{\text{all
possible states}} \exp(-\mathcal{H})
$$

The free energy of a system can be determined as 
$$
F = - \ln(Z).
$${#eq:freeenergy}

All other observables of interest can be derived from the free energy through
Maxwell relations [@pathria2011]. What makes it extremely hard to pursue these
computations analytically is the divergent cardinality of the configurations: we
will then use numerical approaches to simulate the physical properties of Ising
spin lattices and sample the configuration space.

## Algorithms

We will use mainly two algorithms: single spin-flip Metropolis algorithm and
Wolff cluster algorithm.

![Comparison of three sequential steps for the single-spin filp algorithm and
the cluster algorithm in the critical region](compcrit.png){width=80%}

Metropolis algorithm
: It is a single spin-flipping algorithm in which configurations are
sequentially generated using a transition probability which depends on the
$\Delta E$ energy difference between states. Each random spin flip is either
approved or rejected according to the Boltzmann distribution which depends on
the energy difference and the equilibrium temperature $T$.

Wolff algorithm
: It is a cluster flipping algorithm, meaning that the probabilistic evaluation
is made on the growth of a cluster, which is then flipped in its entirety. In
this case at least one spin flip occurs at each timestep.

### Markov process and Markov chain

Both of the algorithms develop the considered system as a discrete stochastic
process made of successive transitions between states that depend only on the
current state of the systems, namely a *Markov process*. Let us give a formal
definition of the latter.

Given $t_1, t_2, ...$ consecutive discrete times, $S_1, S_2, ...$ possible
states of our systems and ${X_t}$ the set of possible states, let us define the
conditional probability that $X_{t_n} = S_{i_n}$ as
$$
P(X_{t_n}=S_{i_n} | X_{t_n-1}=S_{i_n-1}, ..., X_{t_1}=S_{i_1}).
$$
If this probability is dependent only on its immediate predecessor
$$
P(X_{t_n}=S_{i_n} | X_{t_n-1}=S_{i_n-1}, ..., X_{t_1}=S_{i_1}) = P(X_{t_n} = 
S_{i_n} | X_{t_n-1}=S_{i_n-1}),
$$
we call the process a *Markov process* and ${X_t}$ is the *Markov chain*.

We construct this conditional probability as a transition probability to move
from state $i$ to state $j$ (*transition matrix*)
$$
P(X_{t_n} = S_{i_n} | X_{t_n-1}=S_{i_n-1}) = W_{ij} = W(S_i \rightarrow S_j),
$$
so that
$$
W_{ij} \geq 0, \; \sum_j W_{ij} = 1.
$$

This way the total probability can be written as
$$
P(X_{t_n} = S_j) = P(X_{t_n} = S_{j} | X_{t_n-1}=S_u) P(X_{t_n-1}=S_i) = W_{ij}
P(X_{t_n-1}=S_i).
$$
We also require the total probability to be conserved and unitary at all times,
and this yields to a null *global balance equation*
$$
\dv{P(S_j, t)}{t} = - \sum_i W_{ji} P(S_j, t) + \sum_i W_{ij} P(S_i, t)=0.
$$

We want these processes to converge to a state of thermodynamical equilibrium in
order for the observables we measure to have physical significance. In order to
reach this condition our Markov chain has to fulfill two important requirements.

Ergodicity
: This principle states that all possible configurations of the system
should be attainable if the process runs for long enough. We hence require all
states to have non-zero probability.

Detailed Balance
: Usually in the importace sampling Monte Carlo process we require a somewhat
stronger condition of *detailed balance*
$$
W_{ji} P_{\text{eq}}(S_j) = W_{ij} P_{\text{eq}}(S_i),
$$
which is computationally easier to solve. The condition states that in
correspondance of every individual process there is a reverse process, and that
in a state of equilibrium the average rate of every process is equal to the
average rate of its reverse process [@lewis1925].

It can be demonstrated that if there exists a distribution that satisfies the
condition of detailed balance, then the distribution is stationary and the
Markov process is reversible. This allows us to construct a system that has the
stationary distribution that we want, and in our case we will use the so called
Boltzmann distribution. In that case we will have, simplifying the notation,
$$
\frac{W_{ij}}{W_{ji}} = \frac{P_j}{P_i} = \exp(-\beta(E_\nu - E_\mu)),
$$ {#eq:boltztrans}
where $\beta=1/k_b T$ and the $E$s are the respective energies of the two
configurations.

### Acceptance ratios

The formulation of an algorithm from the foundations of the Markov process does
not seem to be straightforward. Let's introduce a concept that will help us to
choose any desired algorithm **and** have a set of appropriate transition
probabilities.

Firstly we will point out that we are allowed to set the business-as-usual
probability non-zero: in fact, in the previous example, @eq:boltztrans with
$\nu=\mu$ yields to $1=1$, which means that this move is **always** valid.

Secondly, we factorize the transition probability in two parts
$$
W_{ij} = g_{ij} A_{ij},
$$
where $g_{ij}$ is the so called *selection probability* and $A_{ij}$ is the
*acceptance ratio*. The *selection probability* is the conditional probability
that given state $i$, the algorithm will generate state $j$. The *acceptance
ratio*, instead, is the fraction of the time while we should accept the new
state and change our system.

If we put the previous definition into @eq:boltztrans, we get the following
$$
\frac{W_{ij}}{W_{ji}} = \frac{g_{ij} A_{ij}}{g_{ji} A_{ji}}.
$${#eq:acceptance}
Since the $A_{ij}/A_{ji}$ can take up any value, also $g_{ji}$ and $g_{ij}$ can
take up any values we like. How we actually proceed, then, is using an algorithm
which generates random new states given old ones with some set of probabilities,
and then we accept those new states with acceptance ratios $A_{ij}$ according to
@eq:acceptance. This kind of algorithm will eventually produce states that agree
with the Boltzmann distribution at equilibrium.


### Metropolis Algorithm

![Efficient acceptance ratio of the Metropolis Algorithm](met_accr.pdf){ width=80% }

In the Metropolis algorithm we choose a set of selection probabilities
$g_{ij}$ for each possible transition and a set of acceptance
ratios in order for @eq:acceptance to statisfy detailed balance. As mentioned
beforehands, the algorithm chooses a new random state, then randomly accepts or
rejects the change according to the chosen acceptance probability. In case of
acceptance the system will go to the new state, otherwise it will be leaved as
it is. The process is repeated many times.

Given the requirements that we defined beforehands, we will choose to use this
acceptance ratio defined as
$$
A_{ij} =
\begin{cases}
\exp(-\beta(E_j-E_i)) & \text{if } E_j-E_i>0\\
1 & \text{otherwise}.
\end{cases}
$$
This means that we will accept every move that brings us to a new state that
has equal or less energy that the actual one. If the new state has greater
energy we will accept it with a certain probability.

In summary the Metropolis single-flip importance sampling Monte Carlo scheme
will be the following:

1. Choose an initial state. In our case it will be a $+1$ cold start.
2. Choose randomly a site $i$.
3. Calculate the energy change $\Delta E$ that would occur if the spin at site
   $i$ were to be overturned.
4. Generate a random number $r\in [0, 1]$.
5. If $r<\exp(-\beta\Delta E)$, flip the spin. Otherwise leave it as it is.
6. Repeat from step 3 until done.

### Wolff Algorithm

Due to the divergence effects of the correlation length and the critical
fluctuations in the critical region, we see the emergence of so called *critical
slowing down* phenomena, that will be properly discussed in the next sections.
In physical terms what happens is that large regions of spins pointing in the
same direction, known as *domains*, emerge. This means that the system is not at
equilibrium, but still it finds it very difficult to go forward in the Markov
chain: in fact spin flips are highly prone to be rejected, being surrounded by
"isles" of uniform spatial spin distribution. The probability of accepting such
a move close to critical temperature [@newman2010] is extremely low, about 3%
regardless of the value of $J$.

![Wolff algorithm clustering process (from [@newman2010])](wolff_algo.png)

Ulli Wolff proposed a solution to this in 1989 [@wolff1989]. The algorithm he
proposed (known as *Wolff algorithm*) is based on the idea that instead of
evaluating and turning over every single spin step by step, spins are grouped in
*clusters* with a certain probability and flipped together at every step. The
Wolff algorithm exceeds the performance of other cluster methods, such as the
Swendsen-Wang algorithm.

The algorithm starts with the random choice of a single site. The neighboring
sites of the chosen spin that have its same orientation are added with
probability $P_{\text{add}} = 1-\exp(-2\beta J)$. Then when no new bonds are to
be formed the entire cluster is flipped at once. The process is then repeated.

![Forward and backward count of breaking links in a Wolff step (from
[@carlon2012])](wolff_mn.pdf){width=70%}

The choice of the mentioned $P_{\text{add}}$ comes from the following: the
selection probabilities are $g_{ij}\propto(1-P_{\text{add}})^m$ and
$g_{ji}\propto(1-P_{\text{add}})^n$, where $m$ and $n$ are respectively the
count of same spin bonds to be broken for a cluster flip and for the reverse
flip. Then the detailed balance condition reads
$$
\frac{W_{ij}}{W_{ji}} = \frac{g_{ij} A_{ij}}{g_{ji} A_{ji}} =
(1-P_{\text{add}})^{m-n} = \exp(-\beta (E_j - E_i)),
$$
and since the energy difference $E_\nu - E_\mu = 2J(m-n)$, if we choose
$P_{\text{add}}$ to be $1-\exp(-2\beta J)$ we have that
$$
\frac{A_{ij}}{A_{ji}} = (\exp(2\beta J) (1-P_{\text{add}}))^{n-m} = 1.
$$
By choosing this $P_{\text{add}}$, the acceptance ratios for both forward and
backward moves are unitary, which is the best value it can take.

Then, in summary, the Wolff cluster scheme will be the following:

1. Choose an initial state. We will choose the $+1$ cold start as stated
   beforehands.
2. Choose randomly a site $i$.
3. Draw bonds to all nearest neighbors that have the same spin with probability
   $P_{\text{add}} = 1-\exp(2\beta J)$.
3. If bonds have been drawn to any nearest neighbor site $j$, draw in turn bonds
   to all nearest neighbors of site $j$ that have the same spin with probability
   $P_{\text{add}} = 1-\exp(2\beta J)$.
4. Repeat step 3 until no more new bonds are created.
5. Flip all spins in the cluster.
6. Go to step 1.

## Critical phenomena {#sec:critph}

It is of our particular interest the study of the Ising model near the critical
temperature $T_c$. In this so called *critical region* typical processes occur,
known by the name of *critical phenomena*. As already mentioned, in this regime
domains of predonminantly $+1$ or $-1$ spins form, producing large fluctuations
in measurements, such as those of the magnetization $m$. It can be seen from the
results [@sec:corrt] that another terrible effect affects our Metropolis
Monte Carlo simulation: for finite size simulations $\tau$ does not diverge, but
it still becomes extremely large in the critical region, making it extremely
difficult to measure independent measures in a reasonably short amount of time,
affecting thus the errors on measurements.

There are two reasons for these behaviors:

- One is due to the finite size of our system. The statistical errors on the
    values we measures are proportional to the size of these critical
    fluctuations. An intuition of why this happen comes from the fact that
    typically clusters can get no bigger than the lattice itself.
- The second reason, the name by which this effect is known being *critical
    slowing down*, is strictly related to the **algorithm** itself that is being
    used to pursue the simulation and not to the system. What we will be doing
    in the next sections is then to study how different algorithms can yield to
    different results in term of critical slowing down and to what extent.

## Finite size scaling

We would like to study the system in its most general setting: in our case it
corresponds to considering the lattice to have an infinite extension. For we
have a limited computational capacity and can only compute numerical solutions
for problems with finite dimensions, we must inevitably make approximations and
simulate a smaller model [@coddington2006].

This introduces systematic errors generally referred to as *finite size
effects*. What we will attempt to do will be then understanding the behavior for
different linear sizes of the system and extrapolating to the infinite system.

As we have already pointed out in [@sec:critph], we would expect a
divergence in the critical zone due to the second order phase transition in the
order parameters (in our case we can refer to the first derivative of the
magnetization $m$). What we see instead in the finite system is a smooth
transition between the paramagnetic and ferromagnetic phase.

The scaling laws that apply to our system are @eq:scalinglaws, and a thorough
treatment of how these equations relate to our measurements is made in
@sec:corrt.

## Periodic Boundary Conditions

![Application of periodic boundary conditions to the two-dimensional Ising model (from [@landau2014])](pbc.png){width=30%}

Since we perform simulations on finite systems and we want them to reproduce
faithfully infinite systems, we have to treat the effects at the boundaries or
"edges" of the system [@landau2014]. In our numerical approximation the
hamiltonian we defined includes only nearest-neighbor interactions: this means
that we can effectively and safely eliminate boundaries by wrapping the
$d$-dimensional lattice on a $(d+1)$-dimensional torus.

This condition is often referred to as *periodic boundary condition*: the first
spin in a row "feels" the interaction with the last spin in the row and
viceversa, and the same happens with the first and last spins in a column.

# Measurements and results

The main observables that we are going to keep track of are the *mean
magentization per site* $\expval{m}$ with respect to the temperature and the
*correlation time* $\tau$ with respect to the size of the lattice.

The mean magnetization per site is defined as
$$
\expval{m} = \frac{M}{N} = \frac{\sum_i^N s_i}{N}
$$
where $N$ is the total number of sites on the lattice and $s_i$ is the value of
the $i$-th spin.

The code to simulate the system and gather the measurements was written in C++.
The code to analyze the data and plot the results was written in Python.
All these resources are available in the project repository
([`https://gitlab.com/shoyip/wolff-project`](https://gitlab.com/shoyip/wolff-project)).

## Magnetization per site

In the single-spin flip case, during the simulation the program kept track of
the magnetization at each step and updated it depending on the acceptance or
rejection of the step. If the move was accepted, the magnetization respectively
increased or decreased by a unit of $2J$ times the considered spin if it was
$-1$ or $+1$.

In the cluster method case also the magnetization was updated at each step, but
it directly counted the number of spins added to the cluster and added or
subtracted of the corresponding count times $2J$ multiplied by the considered
spin.

In both cases the measures of the mean magnetization per site $\expval{m}$ were
recorded in a file each 100 timesteps associated to the corresponding timestep
per lattice size (also called *sweep*). The recording started after a number of
equilibration steps: it was chosen to adopt the correlation time as the
threshold [@newman2010, p.59].

![Comparison between results of mean magnetization per site with respect to the
temperature for different algorithms and sizes and analytical solution](fig2.pdf){width=70%}

![Absolute errors on mean magnetization measurements for $L=10$](fig4.pdf){width=70%}

![Absolute errors on mean magnetization measurements for $L=100$](fig3.pdf){width=70%}

## Correlation time {#sec:corrt}

Magnetization data was recorded after equilibration for systems with increasing
lattice size. Then correlation time was calculated over the magnetization
by fitting the data with an exponentially decreasing function.

The autocorrelation function [@guthrie2020] is found by cross-correlating the magnetization
time serie with itself, which in turn implies the use of Fast Fourier Transforms
as described in [@newman2010]. The results are affected by a non-negligible
error, probably due to fluctuations of the algorithm.

Then correlation times $\tau$ are plotted against lattice sizes $L$ and,
subsequently linear fit between the $\log$ values of these is made. 

Due to the scaling laws
$$
\xi \sim |t|^{-\nu}, \; \tau \sim |t|^{-z\nu},
$${#eq:scalinglaws}
where $\xi$ is the correlation length, $t$ is the reduced temperature defined as
$t=(T-T_c)/T$ and $\nu$ and $z$ are *critical exponents*, we have the following
relation
$$
\tau \sim \xi^z.
$$
We expect that a high value of $z$ corresponds to a fast increase in the
correlation time $\tau$ as size goes to infinity in the critical region.

![Linear fit over loglog values of correlation time and lattice side length for
the Metropolis Monte Carlo algorithm](fig5.pdf){width=70%}

![Linear fit over loglog values of correlation time and lattice side length for
the Wolff algorithm](fig6.pdf){width=70%}

The linear fit yields to the following $z$ values:
$z_{\text{Met}}=\SI{3.0(2)}{}$ for the Metropolis Monte Carlo algorithm and
$z_{\text{Wolff}}=\SI{0.3(2)}{}$ for the Wolff algorithm, and both of them are
compatible with the respective values found in literature, respectively
$z^{\text{th}}_{\text{Met}}=\SI{2.1665(12)}{}$ from [@nightingale1996] and
$z^{\text{th}}_{\text{Wolff}}=\SI{0.25(1)}{}$ from [@coddington1992], with
a confidence of $5\sigma$.

# Renormalization Group and Machine Learning applications to the problem

Given the difficulties reported in the previous sections mainly due to the
phenomenon of critical slowing down, we seek for better algorithmic solutions.
Such approaches may be found starting from the intuitions of physicist Leo
Kadanoff.

## Widom's and Kadanoff's approach to Renormalization

The work of Leo Kadanoff [@kadanoff1966] follows from Widom's scaling hypothesis
[@widom1965] according to which one can verify experimentally the following
critical exponent relations
$$
\begin{cases}
\gamma + 2\beta = 2-\alpha\\
\delta = \frac{\gamma}{\beta} + 1\\
\dd{\nu} = 2 - \alpha\\
\gamma = (2-\eta) \nu.
\end{cases}
$$

The findings from Widom suggested that those parameters were universal, because
they depend solely on the symmetry of the order parameter (in our case the
$\mathbb{Z}_2$ symmetry of magnetization) and the dimensionality of space.
Critical exponents do not depend on lattice structure, the range or the form of
the interactions.

Kadanoff formulates a physical model to explain this kind of behavior for an
Ising model near criticality. The intuition behind his explanation is the
following: when interactions are local, smaller scales are less important so the
averaging process can be operated over scales that are somewhat shorter than the
typical dimension of the system. This is because as large distance fluctuations
become dominant in the behavior of the system, they wash out more detailed
properties of the system. If this hypothesis is true then all systems near
criticality should behave qualitatively the same way, hence have the same
critical exponents and be described by the same theory [@jarrell2017].

## Outline of the Kadanoff Variational RG Hypothesis

Let us consider an Ising model on a lattice with spacing $a$, with a hamiltonian
of the form @eq:hamilt. We expect a second order phase transition to take place
for $T=T_c, H=0$.

Near criticality we assume that an effective theory describing large-scale
fluctuations is dominant. We try to formalize this theory by introducing new
degrees of freedom that describe the properties *droplets* and by grouping them
into spin blocks of linear size $ba$ containing $b^2$ spins in each block. Then
we have the *spin block variable*
$$
\widetilde{s_k} = \zeta \sum_{i_k}^{b^2} s_{i_k}
$$
where $\zeta$ is a variational parameter, $k$ refers to a spin block and $i$ is
the $i$-th spin enclosed in the $k$ block.

![Coarse-graining of an Ising spin system by the introduction of new block
variables](spinblock.pdf){width=50%}

In order to eliminate short distance fluctuations we fix $\widetilde{s_k}$ for
each $k$-th block. We have to verify that the partition function will remain the
same even after the redefinition of variables
$$
Z = \sum_{\{s_i\}} \exp(-\beta\mathcal{H}(s_i)) = \sum_{\{\widetilde{s}_i\}} \exp(-\beta\widetilde{\mathcal{H}}(\widetilde{s}_i))
$$
so we have that our new exponential term should be written as
$$
\exp(-\beta \widetilde{\mathcal{H}}(\widetilde{s}_i)) = \sum_{\{s_i\}} \prod_k
\delta \left( \widetilde{s_k} - \zeta \sum_{i_k}^{b^2} s_{i_k} \right)
\exp(-\beta\mathcal{H}(s_i)).
$$

We make the assumption that only nearest-neighbor interactions are taken into
account. Thus the resulting hamiltonian can be written as
$$
\widetilde{\mathcal{H}} = - \sum_{\expval{ij}} J_b \widetilde{s}_i \widetilde{s}_j
+ H_b \sum_i \widetilde{s}_i.
$$

The new correlation length is
$$
\xi (J, H) = b \, \xi(J_b, H_b) = \xi_b
$$

We finally have modeled a new interaction model with lattice spacing $ba$ and
lattice variables $s_k$, having though the same partition function and hence the
same free energy. What we have accomplished is then a new description of the
system that has a different coupling constant $J_b$ closer to criticality than
the original one, and a shorter correlation length $\xi_b= \xi/b$.

In Kadanoff's scheme we should find parameter $\zeta$ so that observables of the
system are invariant for coarse-graining. We can generalize this scheme to a
model that includes other variational parameters $\lambda$, and express the
operator that connects $s_i$ to $\tilde{s}_k$ variables as $\mathbf{T}_\lambda
({s_i}, {\tilde{s}_k})$. Then the procedure of finding $\zeta$ translates into
the minimization of the free energy difference $\Delta F = \tilde F_\lambda -
F$, where each free energy is calculated as in @eq:freeenergy.

Introductions to Renormalization Groups theory and its applications to lattice
systems can be found on [@jarrell2017] for what concerns the work of Leo
Kadanoff and on [@wilson1979]. It is not our goal to give a thorough description
of the RG theory, so we will not elaborate further on this topic. Instead we
want to understand how these concepts relate to new algorithms and computational
techniques commonly being referred to as *Deep Learning*
techniques. This relation is pointed out in the seminal work [@mehta2014] by
Mehta and Schwab.

## Introduction to Restricted Boltzmann Machines and mapping with variational RG

The paper [@mehta2014] by Mehta and Schwab focuses on the mapping of concepts
from Renormalization Group theory and a specific class of Deep Learning
algorithms called Restricted Boltzmann Machines (RBMs).

RBMs are a class of stochastic neural networks and they have nowadays a
plethora of applications mostly in generative models of data such as movie
ratings, descriptive word bags of documents, labeled or unlabeled images. The
most important use of RBMs is as building blocks of so called *deep belief
networks* [@salakhutdinov2015].

![Graph models of a Restricted Boltzmann Machine and of a two-hidden layer Deep
Belief Network](rbmdbn.pdf)

RBMs [@hinton2010] have two layers: the *visible layer* is the one with units of
known state, and the *hidden layer* encodes features detected. Each unit of the
visible and hidden layers are in a binary state. A joint
configuration $(\vec v, \vec h)$ has an *energy* that tells us how
unlikely is for the given configuration to be verified
$$
E(\vec v, \vec h) = - \sum_i a_i v_i - \sum_j b_j h_j - \sum_{i, j} v_i h_j
w_{ij}
$${#eq:rbm_erg}
where $v_i$ and $h_j$ are the binary states of the visible and hidden units,
$\lambda = \{ a_i, b_j, w_{ij}$ are the variational parameters of the model. A configuration $(\vec v, \vec h)$ has a
probability given by the Boltzmann distribution
$$
p_\lambda(\vec v, \vec h) = \frac{1}{Z} \exp(-E(\vec v, \vec h))
$$
where
$$
Z = \sum_{(\vec v, \vec h)} \exp(-E(\vec v, \vec h)).
$$

![Gibbs sampling procedure (taken from [@d2020learning])](gibbs_sampling.pdf)

What operatively-speaking happens when RBMs are put into action [@oppermann2020]
can be divided in two parts:

1. **Gibbs sampling.** Given $\vec v$ we use $p(\vec h | \vec v)$ to predict the
   hidden values $\vec h$, then knowing the hidden values $\vec h$ we use
   $p(\vec v | \vec h)$ to predict new input values $\vec v$. We repeat this
   process $k$ times, and then after $k$ iterations we have the new input vector
   $v_k$ recreated from $v_0$.
2. **Contrastive divergence.** Now we should update the weight matrix. Given the
   activation functions (the functions that determine whether the unit will be 1
   or 0, usually given by functions such as $\tanh$, the sigmoid or the ReLU
   [@wiki_actfx]), we know how to find vectors $h_k$ and $h_0$. The update
   matrix is then the difference between the outer product between the
   respective vectors and conditional probabilities:
   $$
   \Delta W = \vec v_0 \otimes p(\vec h_0 | v_0) - \vec v_k \otimes p(\vec h_k | v_k)
   $$
   Then the updated weight matrix is calculated as $W = W_0 + \Delta W$.
   Contrastive divergence is extremely useful to us, because the evaluation of
   exact maximum likelihood in this model is intractable due to the exponential
   time that it requires in the number of visible or hidden variables
   [@salakhutdinov2015].

![RG and deep learning analogies in the one-dimensional Ising model (taken from [@mehta2014])](rg_rbm_decimation.png)

Mehta and Schwab argue [@mehta2014] that the joint energy function @eq:rbm_erg
plays the same role of the $\mathbf{T}_\lambda ({s_i}, {\tilde{s}_k})$ operator,
and that the one-to-one mapping is given by the following equation
$$
\mathbf{T} (\vec v_j, \vec h_j) = - E(\vec v, \vec h) + \mathcal{H}[\vec v]
$$
where $\mathcal{H}[\vec v]$ is the hamiltonian that encodes the probability
distribution of visible units $p(\vec v)$.

This way the Renormalization Group Hamiltonian $\widetilde{\mathcal{H}}$, given
by the redefinition of variables due to coarse graining, also describes the
hidden units in the RBM. It can be then proven that the marginal distribution
$p(\vec h)$ has a Boltzmann form with a hamiltonian $\widetilde{\mathcal{H}}$.
Also the  $\mathbf{T} (\vec v_j, \vec h_j)$ operator can be interpreted as a
variational approximation for the conditional probability $p(\vec h |\vec v)$.

The only difference between the RG approach and the RBM approach rises when we
acknowledge that the variational RG transformation cannot be exactly performed.
Instead the variational approach takes into account a family of approximations.

This is an introduction to get a hint of what can be done in terms of Machine
Learning algorithms in order to improve the sampling of physical properties from
a two-dimensional Ising system.

## Restricted Boltzmann Machines Learning of the Ising Model{#sec:rbm}

In what follows we will be reproducing the work performed by D'Angelo and
Böttcher in [@d2020learning] and try to create a generative network for Ising
configurations. The code used for the functions implemented in this elaboration
were taken from a Github repository [@dangelo_repo].

Using the `tensorflow` library two neural networks are created:

1. First and foremost an RBM is structured with $N_v$ visible units
   corresponding to the number $N$ of lattice spin sites (in our case
   $N_v=N=32^2=1024$) and $N_h < N_v$ hidden units (see @iso2018 for a
   justification of the inequality in order to get physically meaningful
   results and avoid overfitting). Our training data, which is a set of
   $\SI{5e3}{}$ spin configurations at each temperature $T \in \{1.0, 2.186995,
   2.261435, 2.2689, 2.269184, 3.0\}$, is feeded into the RBM. Once the training is completed, our RBM
   will then generate samples of spin configurations.
2. Then a convolutional neural network is created and properly trained on a
   $L=32$ system in order to recognize the temperature of a given configuration
   (thus the network is classified as a *supervised learning* algorithm).

Combining the two we were able to evaluate the quality of the learning process
by the RBM. As [@d2020learning] points out, training the RBM is more challenging
and computationally expensive than Variational Autoencoders (VAEs). However the
formers yield to more uniform relative frequencies between temperature classes
with respect to VAEs, which struggle to find configurations near criticality.
VAEs may be more complex than RBMs but in the case of the Ising problem they may
be an overkill.

Another important aspect highlighted in [@iso2018; @d2020learning] is that for
the three types of RBMs, that Iso et al. call Type-V, Type-H and Type-L,
respectively trained for the whole spectrum of temperatures, higher temperatures
(paramagnetic phase) and the lower temperatures (ferromagnetic phase), show
similar performances.

In the RBM the following parameters were used:

$N_v$  | $N_h$  | Batch | CD | Optimizer   | L1 Parameter | Learning Rate | Epochs |
-------|--------|-------|----|-------------|--------------|---------------|--------|
1024   | 800 | 128 | $k$-CD | ADAM | \SI{-1e-6}{} | \SI{1e-3}{} | 100                  |

We have simulated 100 configurations of the two-dimensional Ising model on an
$L=32$ lattice, associated a temperature to each configuration with a CNN and
then plotted the mean magentization per site for each temperature. The results
are plotted in @fig:rbmmag.

![Mean Magnetization per Site as a function of temperature for each
configuration generated by the RBM](RBM_mag.pdf){width=50%; #fig:rbmmag}

The quality of the classification CNN can be evaluated by looking at the
accuracy, which is defined as the *number of correct predictions* divided by
the *number of total predictions*, which even after 100 epochs reaches a value
of 0.3. This means that every prediction that the CNN makes, nearly two thrids
of the times it will get the classification wrong.

Another way to visualize this is by feeding configurations of known temperature
to the CNN and looking at the classification occurences per temperature for each
configuration class.

![Classification occurences for each temperature class of configurations](class_accuracy.pdf){width=50%; #fig:classacc}

As we can clearly see in @fig:classacc, for configurations nearer to the
critical zone the occurrences peak less on the temperature spectrum, hence
explaining the low accuracy espencially for configurations near criticality.

Also the RBM with an even distribution of training configurations over the
temperature spectrum (instead of training it with a higher density of
configurations in criticality) has problems, as we can see in @fig:rbmconfigs.
Of 100 configurations that were generated after 5000 Monte Carlo steps, only two
seem to have a hint of clustering and for that reason the RBM generates results that are
not informative over the critical region.

![Sample configurations generated by the RBM trained evenly over the temperature spectrum](4040_configs.png){#fig:rbmconfigs}

# Conclusions

We have reviewed in summary all the important physical aspects of the Ising
problem, the numerical approaches to study it, the implications of the different
approximations and algorithms, the performances of the latters. We then have
measured the numerical results of the simulations of the two-dimensional Ising
model using the single-spin flip Monte Carlo dynamics and the Wolff clustering
update approach.

We compared the absolute error values for the $L=10$ and $L=100$. In both cases
we notice clearly that simulations with Wolff cluster updates carry less
variance on the order parameter than those updated with single-spin flips.

We can say that the experimental results, though carrying a substantial
variance, are compatible with the ones presented in [@nightingale1996] and
[@coddington1992]. It is verified that the errors scale linearly with decreasing
linear size of the system: this consideration supports the validity of our results.

What can be further done in order to speed up the simulations in terms of CPU
time is parallelizing the processes over multiple cores. Also more recent
technological advances have been made, and AI accelerators such as Tensor
Processing Units can be used [@yang2019] in order to get better performances.

Furthermore, we have theoretically introduced Kadanoff's Variational
Renormalization Group approach to the Ising problem and made a comparison
between the latter and the Restricted Boltzmann Machine algorithm, first
introduced by Paul Smolensky [@rumelhart1999] and then mastered by Geoffrey
Hinton [@salakhutdinov2007; @hinton2010].

Following this justification we have performed training and generation of new
Ising samples reproducing the steps described in D'Angelo and Böttcher's work
[@d2020learning].

These results are not completely satisfactory. We do not get a uniform
distribution over the temperatures of the configurations generated by the RBM,
but we have good results over the critical region. This may be due
to an unsatisfactory accuracy of the CNN, especially over criticality as we saw
in @sec:rbm. Other factors that may have influenced
the result could be the presence of boundaries, and the uneven
distribution of the training configurations in terms of temperature. We also
implemented a second training with sets of configurations evenly distributed
over the temperature spectrum, but that instead yielded to results that were
uninformative over the critical region. This suggests us that
we may obtain better results if we populated the training set with
configurations over the whole temperature spectrum and a higher density over the
critical zone.

What could be done to further improve these result? Firstly understanding if there are
patterns emerging from different training sessions and the generated
configurations could help to prune off unphysical results. Also a Grid Search Cross Validation over a range of
hyperparameters could be implemented, in order to tweak the algorithm in the
optimal way. In a sense, this might be time consuming but theoretically correct
as a procedure, if we stick to the "no free lunch" theorem as a guiding
principle.

Surely more theoretically and numerically advanced approaches to the
two-dimensional Ising problem can be found. Wang has described [@wang2016] a manner of speeding the MCMC algorithm up by alternating ordinary
steps with surrogate configurations generation steps by RBMs, Iso et al. have
studied [@iso2018] the properties of RBM flows given different hyperparameters
and Alexandrou et al. have used Autoencoders [@alexandrou2020] to tackle the
problem.

There is a lot of attention towards this problem, approached in many different
ways that span from purely numerical "simple" Monte Carlo Markov Chain processes
to supervised and unsupervised ML approaches. What is surprising of the latter
ones
is that from the mere analysis of a limited number of samples, neural networks
can learn the physical properties of a system, storing the underlying
informations in so-called *hidden layers*. Though the two-dimensional Ising
model has a (non-trivial) analytically-known solution, it is a good starting
point in order to understand how powerful these new computational approaches are
in recognizing patterns that the human brain or eye may not catch at first
glance.

# Bibliography
